package com.example.Transaction;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.example.Transaction.Student;
import com.example.Transaction.StudentRepository;

@Service
public class StudentService {

	@Autowired
	private StudentRepository studentRepository;

	@Transactional
	public Student saveStudent(Student student) {
		printTransactionInfo();
		return studentRepository.save(student);
	}

	private void printTransactionInfo() {
		if (TransactionAspectSupport.currentTransactionStatus().isNewTransaction()) {
			System.out.println("new transaction");
		} else {
			System.out.println("old transaction");
		}
	}
	@Transactional
	public Student updateStudent(Student s,int id)
	{
		
		s.setId(id);
		Student student=studentRepository.saveAndFlush(s);
		int a=10/0;
		System.out.println(a);
		return student;
	}

	@Transactional(isolation = Isolation.READ_COMMITTED)
	public List<Student> getStudent() {
		printTransactionInfo();
		return studentRepository.findAll();
	}

	

	public void deleteStudent(Integer id) {
		printTransactionInfo();
		studentRepository.deleteById(id);
	}

}

//public class StudentService {
//
//}
