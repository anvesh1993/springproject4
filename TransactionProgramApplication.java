package com.example.Transaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransactionProgramApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransactionProgramApplication.class, args);
	}

}
