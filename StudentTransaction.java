package com.example.Transaction;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
@SpringBootApplication
@EnableTransactionManagement
//@EnableJpaRepositories


public class StudentTransaction {

	public static void main(String[] args) {
		SpringApplication.run(StudentTransaction.class, args);
	}

}