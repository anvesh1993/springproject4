package com.example.Transaction;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class StudentController 
{
	@Autowired
	private StudentService studentService;
	
	@GetMapping(value="students1")
	public ResponseEntity<List<Student>> getAllStu()
	{
		List<Student> li=studentService.getStudent();
		return ResponseEntity.status(200).body(li);
	}
	
	@PostMapping(value="students1")
	public ResponseEntity<Student> addStu(@RequestBody Student stu)
	{
		Student s=studentService.saveStudent(stu);
		return ResponseEntity.status(201).body(s);
		
	}
	@PutMapping(value="/students/{id}")
	public ResponseEntity<Student> updateStudent(@RequestBody Student stu,@PathVariable("id") int id)
	{
		return ResponseEntity.status(201).body(studentService.updateStudent(stu, id));
		
	}

}
