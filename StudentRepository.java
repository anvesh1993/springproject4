package com.example.Transaction;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.Transaction.Student;

public interface StudentRepository extends JpaRepository<Student, Integer>
{

}